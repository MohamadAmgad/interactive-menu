function searchTree(obj,search,path){

    if(obj.name === search){ 
        path.push(obj);
        return path;
    }
    else if(obj.children || obj._children){
        var children = (obj.children) ? obj.children : obj._children;
        var foundItems = [];

        for(var i=0;i<children.length;i++){

            path.push(obj);

            var found = searchTree(children[i],search,path);

            if(found){
                found = found.filter(f => (f.id) && (f.id !== 4))
                displayMenuItems(found);
                foundItems.push(found);
            }
            else{
                path.pop();
            }
        }
    }
    else{
        return false;
    }
}

function displayMenuItems(items){
    var ul = document.getElementById("list");
    var li;
    items.forEach(function(item) {
        li = document.createElement("li");
        var a = document.createElement('a');
        li.onclick=function check() {
            document.getElementById("list").style.display="none";
            document.getElementById("tree").style.display="block";
            openPaths([item])
        }
        a.appendChild(document.createTextNode(item.name));
        li.appendChild(a);

    });
    ul.appendChild(li);
}

function extract_select2_data(node,leaves,index){
        if (node.children){
            for(var i = 0;i<node.children.length;i++){
                index = extract_select2_data(node.children[i],leaves,index)[0];
            }
        }
        else {
            leaves.push({id:++index,text:node.name});
        }
        return [index,leaves];
}

var div = d3.select("body")
    .append("div")
    .attr("class", "tooltip")
    .style("opacity", 0);

var margin = {top: 20, right: 120, bottom: 20, left: 120},
    width = 960 - margin.right - margin.left,
    height = 800 - margin.top - margin.bottom;

var i = 0,
    duration = 750,
    root,
    select2_data;

var diameter = 960;

var tree = d3.layout.tree()
    .size([height, width]);

var diagonal = d3.svg.diagonal()
    .projection(function(d) { return [d.y, d.x]; });

var svg = d3.select(".tree").append("svg")
    .attr("width", width + margin.right + margin.left)
    .attr("height", height + margin.top + margin.bottom)
      .append("g")
    .attr("transform", "translate(" + margin.left + "," + -40 + ")");


function collapse(d) {
    if (d.children) {
        d._children = d.children;
        d._children.forEach(collapse);
        d.children = null;
    }
}


function click(d) {
    if (d.children) {
        d._children = d.children;
        d.children = null;
      }
      else{
        d.children = d._children;
        d._children = null;
      }
    update(d);
}

function openPaths(paths){
    for(var i =0;i<paths.length;i++){
        if(paths[i].id !== "1"){
            paths[i].class = 'found';
            if(paths[i]._children){
                paths[i].children = paths[i]._children;
                paths[i]._children = null;
            }
            update(paths[i]);
        }
    }
}

function onlyUnique(value, index, self) { 
    return self.indexOf(value) === index;
}


d3.json("flar.json", function(error,values){
    document.getElementById("search").innerHTML ='adfg';
    root = values;
    select2_data = extract_select2_data(values,[],0)[1];
    const uniqueAddresses = Array.from(new Set(select2_data.map(a => a.text))).map(text => {
        return select2_data.find(a => a.text === text)
    })
    root.x0 = height / 2;
    root.y0 = 0;
    root.children.forEach(collapse);
    update(root);
    $("#search").select2({
        data: uniqueAddresses,
        containerCssClass: "search"
    });
});

$("#search").on("select2-selecting", function(e) {
    document.getElementById("list").innerHTML = '';
    var paths = searchTree(root,e.object.text,[]);
    if(typeof(paths) !== "undefined"){
        openPaths(paths);
    }
})

d3.select(self.frameElement).style("height", "800px");

function update(source) {

    var nodes = tree.nodes(root).reverse(),
    links = tree.links(nodes);


    nodes.forEach(function(d) { d.y = d.depth * 180; });


    var node = svg.selectAll("g.node")
        .data(nodes, function(d) { return d.id || (d.id = ++i); });


    var nodeEnter = node.enter().append("g")
        .attr("class", "node")
    .attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
    .on("click", click);

    nodeEnter.append("circle")
    .attr("r", 1e-6)
    .style("fill", function(d) { return d._children ? "#f2f2f2" : "#fff"; });

    nodeEnter.append("text")
        .attr("x", function(d) { return d.children || d._children ? -10 : 10; })
        .attr("dy", ".35em")
        .attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
        .text(function(d) { return d.name; })
        .style("fill-opacity", 1e-6);


    var nodeUpdate = node.transition()
        .duration(duration)
        .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

    nodeUpdate.select("circle")
        .attr("r", 4.5)
        .style("fill", function(d) {
            if(d.class === "found"){
                return "#0ad498";
            }
            else if(d._children){
                return "#f2f2f2";
            }
            else{
                return "#fff";
            }
        })
        .style("stroke", function(d) {
            if(d.class === "found"){
                return "#0ad498";
            }
    });

    nodeUpdate.select("text")
        .style("fill-opacity", 1);


    var nodeExit = node.exit().transition()
        .duration(duration)
        .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
        .remove();

    nodeExit.select("circle")
        .attr("r", 1e-6);

    nodeExit.select("text")
        .style("fill-opacity", 1e-6);


    var link = svg.selectAll("path.link")
        .data(links, function(d) { return d.target.id; });


    link.enter().insert("path", "g")
        .attr("class", "link")
        .attr("d", function(d) {
            var o = {x: source.x0, y: source.y0};
            return diagonal({source: o, target: o});
        });


    link.transition()
        .duration(duration)
        .attr("d", diagonal)
        .style("stroke",function(d){
            if(d.target.class==="found"){
                return "#0ad498";
            }
        });


    link.exit().transition()
        .duration(duration)
        .attr("d", function(d) {
            var o = {x: source.x, y: source.y};
            return diagonal({source: o, target: o});
        })
        .remove();


    nodes.forEach(function(d) {
        d.x0 = d.x;
        d.y0 = d.y;
      });
}